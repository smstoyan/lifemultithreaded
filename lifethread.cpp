#include "lifethread.h"
#include <QPixmap>

LifeData *LifeThread::data = NULL;

LifeThread::LifeThread(QObject *parent) :
    QObject(parent)
{

}

void LifeThread::checkHorizontalBorder(register unsigned int *cellsToCheck, int tmpW, register unsigned int *neighborsNext, register unsigned int *neighborsPrev, int diffToBotCells, int chunkWidth, int y)
{
    for(unsigned int i = 1; i < tmpW; i++)
    {
        QColor color = Qt::white;
        if ((*cellsToCheck)==1)
        {
            if ((*neighborsPrev == 2) || (*neighborsPrev == 3))
            {
                (*(neighborsNext + 1))++;
                (*(neighborsNext - 1))++;

                (*(neighborsNext + chunkWidth - 1))++;
                (*(neighborsNext + chunkWidth))++;
                (*(neighborsNext + chunkWidth + 1))++;

                (*(neighborsNext + diffToBotCells - 1))++;
                (*(neighborsNext + diffToBotCells))++;
                (*(neighborsNext + diffToBotCells + 1))++;
                color = Qt::black;
            }
            else
            {
                (*cellsToCheck) = 0;
            }
        }
        else
        {
            if ((*neighborsPrev) == 3)
            {
                (*cellsToCheck) = 1;

                (*(neighborsNext + 1))++;
                (*(neighborsNext - 1))++;

                (*(neighborsNext + chunkWidth - 1))++;
                (*(neighborsNext + chunkWidth))++;
                (*(neighborsNext + chunkWidth + 1))++;

                (*(neighborsNext + diffToBotCells - 1))++;
                (*(neighborsNext + diffToBotCells))++;
                (*(neighborsNext + diffToBotCells + 1))++;
                color = Qt::black;
            }
        }
        painter.fillRect(i,y,1,1,color);
        (*neighborsPrev) = 0;
        cellsToCheck++;
        neighborsNext++;
        neighborsPrev++;
    }
}

void LifeThread::checkVerticalBorder(unsigned int *cellsToCheck, int tmpH,
                                     unsigned int *neighborsNext, unsigned int *neighborsPrev,
                                     int diffToCells, int chunkWidth, int x)
{
    for (int i = 1; i < tmpH; i++)
    {
        QColor color = Qt::white;
        if ((*cellsToCheck) == 1)
        {
            if (((*neighborsPrev) == 2) || ((*neighborsPrev) == 3))
            {
                (*(neighborsNext-diffToCells))++;
                (*(neighborsNext-chunkWidth))++;
                (*(neighborsNext-chunkWidth+diffToCells))++;

                (*(neighborsNext+chunkWidth-diffToCells))++;
                (*(neighborsNext+diffToCells))++;

                (*(neighborsNext + chunkWidth + chunkWidth - diffToCells))++;
                (*(neighborsNext+chunkWidth))++;
                (*(neighborsNext+chunkWidth+diffToCells))++;

                color = Qt::black;
            }
            else
            {
                (*cellsToCheck) = 0;
            }
        }
        else
        {
            if ((*neighborsPrev)==3)
            {
                (*cellsToCheck) = 1;

                (*(neighborsNext-diffToCells))++;
                (*(neighborsNext-chunkWidth))++;
                (*(neighborsNext-chunkWidth+diffToCells))++;

                (*(neighborsNext+chunkWidth-diffToCells))++;
                (*(neighborsNext+diffToCells))++;

                (*(neighborsNext + chunkWidth + chunkWidth - diffToCells))++;
                (*(neighborsNext+chunkWidth))++;
                (*(neighborsNext+chunkWidth+diffToCells))++;

                color = Qt::black;
            }
        }
        painter.fillRect(x, i, 1,1, color);
        (*neighborsPrev) = 0;
        neighborsNext += qAbs(chunkWidth);
        neighborsPrev += qAbs(chunkWidth);
        cellsToCheck += qAbs(chunkWidth);
    }
}

void LifeThread::process()
{
    bool work = true;
    unsigned int *p;
    unsigned int *prev;
    unsigned int *next;
    unsigned int chunkWidth;
    unsigned int chunkHeight;
    unsigned int lineWidth;
    unsigned int chNum;

    data->getAccessToBorder(&p,&prev, &next,&chunkWidth,&chunkHeight,&work);
    if (work)
    {
        unsigned int *cellsToCheck = p+1;
        unsigned int *neighborsPrev = prev+1;
        unsigned int *neighborsNext = next+1;

        //top cells
        int tmpW = chunkWidth - 1;
        int tmpH = chunkHeight - 1;
        int diffToCells = chunkWidth * tmpH;
        checkHorizontalBorder(cellsToCheck, tmpW, neighborsNext, neighborsPrev, diffToCells, chunkWidth, 0);

        //bot cells
        cellsToCheck += diffToCells;
        neighborsNext += diffToCells;
        neighborsPrev += diffToCells;
        checkHorizontalBorder(cellsToCheck, tmpW, neighborsNext, neighborsPrev, -diffToCells, -chunkWidth, chunkHeight);

        //left cells
        cellsToCheck = p + chunkWidth;
        neighborsNext = next + chunkWidth;
        neighborsPrev = prev + chunkWidth;
        diffToCells = 1;
        checkVerticalBorder(cellsToCheck, tmpH, neighborsNext, neighborsPrev, diffToCells, chunkWidth, 0);

        //right cells
        cellsToCheck--;
        neighborsNext--;
        neighborsPrev--;
        checkVerticalBorder(cellsToCheck, tmpH, neighborsNext, neighborsPrev, -diffToCells, -chunkWidth, chunkWidth);
    }

    painter.begin(data->getPixmap());
    unsigned int x, y;
    data->getNextChunkParams(&chNum, &p,
                             &prev,&next,
                             &chunkWidth,&chunkHeight,
                             &x, &y,
                             &lineWidth,&work);
    while(work)
    {
        for(unsigned int i = 0; i < chunkHeight; i++)
        {
            unsigned int *bp = p;
            unsigned int *bprev = prev;
            unsigned int *bnext = next;
            for(unsigned int j = 0; j < chunkWidth; j++)
            {
                QColor color = Qt::white;
                if ((*bp)==1)
                {
                    if(((*bprev) != 2) && ((*bprev) != 3))
                    {
                        (*bp) = 0;
                    }
                    else
                    {
                        (*(bnext + 1))++;
                        (*(bnext - 1))++;
                        unsigned int *tp, *bp;
                        tp = bnext - lineWidth;
                        bp = bnext + lineWidth;
                        (*tp)++;
                        (*bp)++;
                        (*(tp+1))++;
                        (*(tp-1))++;
                        (*(bp+1))++;
                        (*(bp-1))++;
                        color = Qt::black;
                    }
                }
                else
                {
                    if ((*bprev) == 3)
                    {
                        (*bp) = 1;

                        (*(bnext + 1))++;
                        (*(bnext - 1))++;
                        unsigned int *tp, *bp;
                        tp = bnext - lineWidth;
                        bp = bnext + lineWidth;
                        (*tp)++;
                        (*bp)++;
                        (*(tp+1))++;
                        (*(tp-1))++;
                        (*(bp+1))++;
                        (*(bp-1))++;
                        color = Qt::black;
                    }
                }
                painter.fillRect(x+j, y+i,1,1,color);
                (*bprev) = 0;
                bp++;
                bprev++;
                bnext++;
                //добавить расчет нового количества соседей
            }
            p += lineWidth;
            prev += lineWidth;
            next += lineWidth;
        }
        data->getNextChunkParams(&chNum, &p,
                                  &prev,&next,
                                  &chunkWidth,&chunkHeight,
                                  &x, &y,
                                  &lineWidth,&work);
    }
    painter.end();
    emit finished();
}

void LifeThread::setData(LifeData *d)
{
    data = d;
}
