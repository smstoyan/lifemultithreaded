#include "lifethreadwrapper.h"

LifeThreadWrapper::LifeThreadWrapper(QObject *parent) :
    QObject(parent)
{
    worker = NULL;
    workerInitialized = false;
    this->moveToThread(&thread);
}

LifeThreadWrapper::~LifeThreadWrapper()
{
    thread.exit(0);
    worker->deleteLater();
}

QThread *LifeThreadWrapper::getThread()
{
    return &thread;
}

void LifeThreadWrapper::createWorker()
{
    if (!worker)
    {
        worker = new LifeThread();
        connect(&thread,SIGNAL(started()),worker,SLOT(process()));
        connect(worker,SIGNAL(finished()),&thread,SLOT(quit()));

        connect(&thread,SIGNAL(finished()),this,SIGNAL(finished()));
    }
}

void LifeThreadWrapper::initThreadWorker()
{
    createWorker();
}
