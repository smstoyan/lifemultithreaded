#include "mw.h"
#include "ui_mw.h"
#include <QThread>

MW::MW(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MW)
{
    ui->setupUi(this);
    lifeWidget = new LifeWidget();
    ui->centralWidget->setLayout(ui->mainLayout);
    lifeWidget->setData(&data);
    ui->lifeWidgetContainer->setWidget(lifeWidget);
    lifeWidget->setPaintAccess(true);
    planner.setData(&data);
    working = false;
    connect(&planner,SIGNAL(stepFinished()),lifeWidget,SLOT(updateFromData()));
    connect(lifeWidget,SIGNAL(repainted()),&planner,SLOT(nextStep()),Qt::DirectConnection);
    connect(&planner,SIGNAL(stopped()),this,SLOT(stopWork()));
    connect(&planner,SIGNAL(stepFinished()),this,SLOT(updateStepsStatus()));
    int cores = QThread::idealThreadCount();
    if (cores > 0)
    {
        ui->threadCount->setValue(cores);
    }
    stepsProcessed = 0;
}

MW::~MW()
{
    delete ui;
}

void MW::on_startButton_clicked()
{
    if (!working)
    {
        startWork(-1,ui->chunkSize->value(), ui->threadCount->value());
        ui->startButton->setText(tr("stop"));
        ui->stepButton->setEnabled(false);
    }
    else
    {
        planner.stop();
        ui->startButton->setEnabled(false);
        ui->startButton->setText(tr("start"));
    }
}

void MW::on_stepButton_clicked()
{
    if (!working)
    {
        startWork(ui->stepsCount->value(), ui->chunkSize->value() ,ui->threadCount->value());
        ui->startButton->setEnabled(false);
        ui->stepButton->setText(tr("stop"));
    }
    else
    {
        planner.stop();
        ui->stepButton->setEnabled(true);
        ui->stepButton->setText(tr("step"));
    }
}


void MW::startWork(int steps, int chunkSize, int threads)
{
    lifeWidget->setPaintAccess(false);
    stepsProcessed = 0;
    working = true;
    ui->stepsCount->setEnabled(false);
    ui->threadCount->setEnabled(false);
    ui->chunkSize->setEnabled(false);
    ui->randomButton->setEnabled(false);
    planner.setThreadCount(threads);
    data.setChunkSize(chunkSize);
    data.setStepsCount(steps);
    data.calculateNeibourgs();
    planner.start(steps);
}

void MW::workEnded()
{
    working = false;
    ui->stepsCount->setEnabled(true);
    ui->threadCount->setEnabled(true);
    ui->chunkSize->setEnabled(true);
    stopWork();
}

void MW::stopWork()
{
    working = false;
    lifeWidget->setPaintAccess(true);
    ui->startButton->setEnabled(true);
    ui->stepButton->setEnabled(true);
    ui->stepsCount->setEnabled(true);
    ui->threadCount->setEnabled(true);
    ui->chunkSize->setEnabled(true);
    ui->randomButton->setEnabled(true);
    ui->stepButton->setText(tr("step"));
}

void MW::on_randomButton_clicked()
{
    lifeWidget->random(ui->probability->value() / 100);
}

void MW::updateStepsStatus()
{
    stepsProcessed++;
    //update statusbar
}
