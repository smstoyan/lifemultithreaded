h#-------------------------------------------------
#
# Project created by QtCreator 2013-11-13T19:14:10
#
#-------------------------------------------------

QT       += core gui #thread

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Life
TEMPLATE = app


SOURCES += main.cpp\
        mw.cpp \
    lifewidget.cpp \
    lifethread.cpp \
    lifethreadwrapper.cpp \
    threadplanner.cpp \
    lifedata.cpp

HEADERS  += mw.h \
    lifewidget.h \
    lifethread.h \
    lifethreadwrapper.h \
    threadplanner.h \
    lifedata.h

FORMS    += mw.ui

#QMAKE_CXXFLAGS_DEBUG += -pg
#QMAKE_LFLAGS_DEBUG += -pg
