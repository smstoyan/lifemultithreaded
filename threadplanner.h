#ifndef THREADPLANNER_H
#define THREADPLANNER_H

#include <QObject>
#include <lifethreadwrapper.h>
#include <QList>
#include <lifedata.h>

class ThreadPlanner : public QObject
{
    Q_OBJECT
public:
    explicit ThreadPlanner(QObject *parent = 0);
    void setData(LifeData *d);
    void setThreadCount(unsigned int n = 1);
    void start(int steps);
signals:
    void stepFinished();
    void stopped();
public slots:
    void nextStep();
    void stop();
private slots:
    void threadFinished();
private:
    QList<LifeThreadWrapper*> threads;
    unsigned int count;
    LifeData *data;
    unsigned int notActiveThreadCount;
    int stepsCount;
    bool continueWorking;

    void deleteThreads(unsigned int n);
    void addThreads(unsigned int n);
};

#endif // THREADPLANNER_H
