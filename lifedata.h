#ifndef LIFEDATA_H
#define LIFEDATA_H

#include <QObject>
#include <QMutex>
#include <QPixmap>

class LifeData : public QObject
{
    Q_OBJECT
public:
    explicit LifeData(QObject *parent = 0);
    ~LifeData();
    void changeChunkSize(unsigned int w, unsigned int h);
    const static int maxHeight;
    const static int maxWidth;
    const static int defaultChunkSize;
    const static int buffersSize;
    void setChunkSize(int size);
    void setStepsCount(int steps);
    unsigned int *getPointer();
    void setCellValue(int x, int y, int v);

    QPixmap *getPixmap();

    void calculateNeibourgs();
    void resetPointers();
    void getAccessToBorder(unsigned int **data, unsigned int **current, unsigned int **next, unsigned int *width, unsigned int *height, bool *notLate);
    void getNextChunkParams(unsigned int *num, unsigned int **p,
                      unsigned int **prev, unsigned int **next,
                      unsigned int *width, unsigned int *height, unsigned int *x, unsigned int *y,
                      unsigned int *lineWidth, bool *work);

signals:

public slots:
private:
    unsigned int chunkWidth, chunkHeight;
    unsigned int chunkNum;
    unsigned int chunkSize;
    unsigned int chunksInLineH,chunksInLineV;
    unsigned int rightChunkWidth;
    unsigned int bottomChunkHeight;

    unsigned int stepsRemaining;
    unsigned int *currBuffer;
    unsigned int *prevBuffer;
    unsigned int *data;
    QMutex dataLocker;
    QPixmap pixmap;

    unsigned int *dataChunk;
    unsigned int *currBufferChunk;
    unsigned int *prevBufferChunk;
    unsigned int chunkPos;
    bool borderIsBusy;
};

#endif // LIFEDATA_H
