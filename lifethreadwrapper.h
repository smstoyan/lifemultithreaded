#ifndef LIFETHREADWRAPPER_H
#define LIFETHREADWRAPPER_H

#include <QObject>
#include <QThread>
#include <lifethread.h>
#include <lifedata.h>

class LifeThreadWrapper : public QObject
{
    Q_OBJECT
public:
    explicit LifeThreadWrapper(QObject *parent = 0);
    ~LifeThreadWrapper();

    QThread *getThread();
    void initThreadWorker();
signals:
    void finished();
public slots:
private:
    QThread thread;
    LifeThread *worker;
    bool workerInitialized;

    void createWorker();
};

#endif // LIFETHREADWRAPPER_H
