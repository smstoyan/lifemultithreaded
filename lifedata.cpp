#include "lifedata.h"
#include "QDebug"

const int LifeData::maxHeight = 300;
const int LifeData::maxWidth = 300;
const int LifeData::defaultChunkSize = 20;
const int LifeData::buffersSize = LifeData::maxHeight * LifeData::maxWidth;

LifeData::LifeData(QObject *parent) :
    QObject(parent)
{
    chunkPos = rightChunkWidth = bottomChunkHeight = 0;
    changeChunkSize(defaultChunkSize,defaultChunkSize);
    currBuffer = new unsigned int [buffersSize];
    prevBuffer = new unsigned int [buffersSize];
    data = new unsigned int[buffersSize];
    stepsRemaining = 0;
    for(int i = 0; i < buffersSize; i++)
    {
        currBuffer[i] = 0;
        prevBuffer[i] = 0;
        data[i] = 0;
    }
    pixmap = QPixmap(maxWidth,maxHeight);
}

LifeData::~LifeData()
{
    delete currBuffer;
    delete prevBuffer;
    delete data;
}

void LifeData::changeChunkSize(unsigned int w, unsigned int h)
{
    unsigned int fwidth = maxWidth - 2;
    unsigned int fheight = maxHeight - 2;
    rightChunkWidth = chunkWidth = w;
    bottomChunkHeight = chunkHeight = h;
    chunksInLineH = fwidth / chunkWidth;
    if (fwidth % chunkWidth)
    {
        rightChunkWidth = fwidth % chunkWidth;
        chunksInLineH++;
    }
    chunksInLineV = fheight / chunkHeight;
    if (fheight % chunkWidth)
    {
        bottomChunkHeight = fheight % chunkWidth;
        chunksInLineV++;
    }
    chunkNum = chunksInLineH * chunksInLineV;
}

void LifeData::getNextChunkParams(unsigned int *num, unsigned int **p,
                                  unsigned int **prev, unsigned int **next,
                                  unsigned int *width, unsigned int *height,
                                  unsigned int *x, unsigned int *y,
                                  unsigned int *lineWidth,
                                  bool *work)
{
    dataLocker.lock();
    if (chunkPos != chunkNum)
    {
        //        qDebug() << dataChunk << "\t" << prevBufferChunk << "\t" << currBufferChunk;
        chunkPos++;
        (*num) = chunkPos;
        (*p) = dataChunk;
        (*prev) = prevBufferChunk;
        (*next) = currBufferChunk;
        (*x) = (dataChunk - data) % maxWidth;
        (*y) = (dataChunk - data) / maxWidth;
        if ((chunkPos % chunksInLineH) == 0)
        {
            (*width) = rightChunkWidth;
            int diff = chunkHeight*maxWidth -(chunksInLineH - 1) * chunkWidth;
            dataChunk += diff;
            currBufferChunk += diff;
            prevBufferChunk += diff;
        } else
        {
            (*width) = chunkWidth;
            dataChunk += chunkWidth;
            currBufferChunk += chunkWidth;
            prevBufferChunk += chunkWidth;
        }
        if (chunkNum - chunkPos < chunksInLineV)
        {
            (*height) = bottomChunkHeight;
        }
        else
        {
            (*height) = chunkHeight;
        }
        (*lineWidth) = maxWidth;
        (*work) = true;
    }
    else
    {
        (*work) = false;
    }
    dataLocker.unlock();
}

void LifeData::setChunkSize(int size)
{
    this->chunkSize = size;
}

unsigned int *LifeData::getPointer()
{
    return data;
}

void LifeData::setStepsCount(int steps)
{
    stepsRemaining = steps;
}

void LifeData::setCellValue(int x, int y,int v)
{
    int pos = maxHeight * y + x;
    data[pos] = v;
}

void LifeData::resetPointers()
{
    chunkPos =  0;
    unsigned int *tmp;
    tmp = currBuffer;
    currBuffer = prevBuffer;
    prevBuffer = tmp;
    qDebug() << "currBuffer:" << currBuffer << "\t" << "prevBuffer:" << prevBuffer;

    borderIsBusy = false;

    dataChunk = data + maxWidth + 1;
    currBufferChunk = currBuffer + maxWidth + 1;
    prevBufferChunk = prevBuffer + maxWidth + 1;
    qDebug() << "currBufferChunk:" << currBufferChunk << "\t"
             << "prevBufferChunk:" << prevBufferChunk;
}

void LifeData::getAccessToBorder(unsigned int **data, unsigned int **current,
                                 unsigned int **next, unsigned int *width,
                                 unsigned int *height, bool *notLate)
{
    dataLocker.lock();
    if (!borderIsBusy)
    {
        (*notLate) = true;
        (*data) = this->data;
        (*width) = maxWidth;
        (*height) = maxHeight;
        (*current) = prevBuffer;
        (*next) = currBuffer;
        borderIsBusy = true;
    }
    else
    {
        (*notLate) = false;
    }
    dataLocker.unlock();
}

void LifeData::calculateNeibourgs()
{
    qDebug() << "neighborgs";
    unsigned int *p = currBuffer;
    for (unsigned int i = 0; i < buffersSize; i++)
    {
        (*p) = 0;
        p++;
    }


    unsigned int mh = maxHeight - 1;
    unsigned int mw = maxWidth - 1;
    unsigned int *n = currBuffer + maxWidth + 1;
    p = data + maxWidth + 1;
    //add borders value count

    for (int i = 1; i < mw; i++)
    {
        register unsigned int *n1tmp = n;
        register unsigned int *n2tmp = n - maxWidth;
        register unsigned int *n3tmp = n + maxWidth;
        register unsigned int *ptmp = p;
        for (unsigned int j = 1; j < mh; j++)
        {
            if ((*ptmp) == 1)
            {
                (*(n1tmp - 1))++;
                (*(n1tmp + 1))++;

                (*n2tmp)++;
                (*(n2tmp - 1))++;
                (*(n2tmp + 1))++;

                (*n3tmp)++;
                (*(n3tmp - 1))++;
                (*(n3tmp + 1))++;
            }
            n1tmp++;
            n2tmp++;
            n3tmp++;
            ptmp++;
        }
        n += maxWidth;
        p += maxWidth;
    }

    //top border
    n = currBuffer + 1;
    p = data + 1;
    unsigned int *b = n + maxHeight * (maxWidth - 1);
    for (unsigned int i = 1; i < mw; i++)
    {
        if((*p)==1)
        {
            (*(n-1))++;
            (*(n+1))++;
            (*(n+maxWidth-1))++;
            (*(n+maxWidth))++;
            (*(n+maxWidth+1))++;
            (*(b-1))++;
            (*b)++;
            (*(b+1))++;
        }
        p++;
        n++;
        b++;
    }

    //bottom border
    p = data + maxHeight * (maxWidth - 1) + 1;
    b = currBuffer + maxHeight * (maxWidth - 1) + 1;
    n = currBuffer + 1;
    for (unsigned int i = 1; i < mw; i++)
    {
        if((*p)==1)
        {
            (*(b-1))++;
            (*(b+1))++;
            (*(b-maxWidth))++;
            (*(b-maxWidth-1))++;
            (*(b-maxWidth+1))++;

            (*(n-1))++;
            (*n)++;
            (*(n+1))++;
        }
        p++;
        n++;
        b++;
    }
    //left border
    p = data + maxWidth;
    b = currBuffer + 2 * maxWidth - 1;
    n = currBuffer + maxWidth;
    for(unsigned int i = 1; i < mh; i++)
    {
        if((*p)==1)
        {
            (*(n+1))++;
            (*(n - maxWidth))++;
            (*(n - maxWidth + 1))++;
            (*(n + maxWidth))++;
            (*(n + maxWidth + 1))++;

            (*(b-maxWidth))++;
            (*b)++;
            (*(b+maxWidth))++;
        }
        p+=maxWidth;
        b+=maxWidth;
        n+=maxWidth;
    }
    //right border
    p = data + 2 * maxWidth - 1;
    b = currBuffer + maxWidth;
    n = currBuffer + 2 * maxWidth - 1;
    for(unsigned int i = 1; i < mh; i++)
    {
        if((*p)==1)
        {
            (*(n-1))++;
            (*(n - maxWidth))++;
            (*(n - maxWidth - 1))++;
            (*(n + maxWidth))++;
            (*(n + maxWidth - 1))++;

            (*(b-maxWidth))++;
            (*b)++;
            (*(b+maxWidth))++;
        }
        p+=maxWidth;
        b+=maxWidth;
        n+=maxWidth;
    }
}

QPixmap *LifeData::getPixmap()
{
    return &pixmap;
}
