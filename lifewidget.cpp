#include "lifewidget.h"
#include <QDebug>
#include <QTimer>

LifeWidget::LifeWidget(QWidget *parent) :
    QWidget(parent)
{
    cellSize = 1;
    canPaint = false;
    data = NULL;
    buffer = QPixmap((1 << cellSize) * LifeData::maxWidth, (1 << cellSize) * LifeData::maxHeight);
    buffer.fill();
    this->setFixedSize(buffer.width() ,buffer.height());
}

void LifeWidget::setData(LifeData *data)
{
    this->data = data;
    repaint();
}

void LifeWidget::mouseMoveEvent(QMouseEvent *ev)
{
    if(data && canPaint)
    {
        switch(ev->buttons())
        {
        case Qt::LeftButton:
            setCellState(ev->x(),ev->y(),true);
            break;
        case Qt::RightButton:
            setCellState(ev->x(),ev->y(),false);
            break;
        default:
            painter.end();
            break;
        }
    }
}

void LifeWidget::mousePressEvent(QMouseEvent *ev)
{
    if(data && canPaint)
    {
        painter.begin(&buffer);
        painter.setBrush(Qt::SolidPattern);
        this->setMouseTracking(true);
        mouseMoveEvent(ev);
    }
}

void LifeWidget::mouseReleaseEvent(QMouseEvent *ev)
{
    if(data)
    {
        Q_UNUSED(ev);
        painter.end();
        this->setMouseTracking(false);
    }
}

void LifeWidget::setCellState(int x, int y, bool alive)
{
    x = x >> cellSize;
    y = y >> cellSize;
    if (checkCoordinates(x,y))
    {
        data->setCellValue(x,y,alive);
        drawCell(x,y,alive);
    }
}

void LifeWidget::drawCell(int x, int y, bool alive)
{
    x = x << cellSize;
    y = y << cellSize;
    QColor finalColor;
    if (alive)
    {
        finalColor = Qt::black;
    }
    else
    {
        finalColor = Qt::white;
    }
    painter.fillRect(x,y,1 << cellSize,1 << cellSize,finalColor);
    this->update();
}

void LifeWidget::paintEvent(QPaintEvent *)
{
    QPainter p;
    p.begin(this);
    p.drawPixmap(0,0,buffer.width(), buffer.height(),buffer);
    p.end();
}

bool LifeWidget::checkCoordinates(int _x, int _y)
{
    return ((_x >= 0) && (_x < LifeData::maxWidth) && (_y >= 0) && (_y < LifeData::maxHeight));
}

void LifeWidget::repaint()
{
    painter.begin(&buffer);
    painter.setBrush(Qt::SolidPattern);
    if (data)
    {
        unsigned int *p = data->getPointer();
        for (int i = 0; i < LifeData::maxHeight; i++)
        {
            for (int j = 0; j < LifeData::maxWidth; j++)
            {
                drawCell(j,i,(*p));
                p++;
            }
        }
    }
    emit repainted();
    qDebug()<<"repainted";
    painter.end();
}

void LifeWidget::updateLifeState()
{
    painter.begin(&buffer);
    painter.setBrush(Qt::SolidPattern);
    if (data)
    {
        unsigned int *p = data->getPointer();
        int px = 0;
        int py = 0;
        for (int i = 0; i < LifeData::buffersSize; i++)
        {
            bool isAlive = (*p);
            setCellState(px,py,isAlive);

            p++;
            py++;
            if (py == LifeData::maxWidth)
            {
                py = 0;
                px++;
            }
        }
    }
    painter.end();
}


void LifeWidget::random(float v)
{
    painter.begin(&buffer);
    painter.setBrush(Qt::SolidPattern);
    int probability = v * RAND_MAX;
    for (register int i = 0; i < LifeData::maxWidth; i++)
    {
        for(register int j = 0; j < LifeData::maxHeight; j++)
        {
            if(qrand() > probability)
            {
                setCellState(i << cellSize, j << cellSize, false);
            }
            else
            {
                setCellState(i << cellSize, j << cellSize, true);
            }
        }
    }
    painter.end();
    this->update();
}

void LifeWidget::setPaintAccess(bool pAccess)
{
    canPaint = pAccess;
}

void LifeWidget::updateFromData()
{

    painter.begin(&buffer);
//    painter.drawImage(QRect(0,0,buffer.width(),buffer.height()),*data->getPixmap());
    painter.drawPixmap(0,0,buffer.width(),buffer.height(),*data->getPixmap());
    painter.end();
    update();
    emit repainted();
    qDebug()<<"repainted";
}
