#include "threadplanner.h"
#include <QDebug>

ThreadPlanner::ThreadPlanner(QObject *parent) :
    QObject(parent)
{
    count = 0;
    data = 0;
    stepsCount = 0;
}

void ThreadPlanner::setThreadCount(unsigned int n)
{
    if(count > n)
        deleteThreads(n);
    else
        addThreads(n);
}

void ThreadPlanner::addThreads(unsigned int n)
{
    while(count!=n)
    {
        LifeThreadWrapper *wrapper = new LifeThreadWrapper();
        connect(wrapper,SIGNAL(finished()),this,SLOT(threadFinished()));
        wrapper->initThreadWorker();
        threads.append(wrapper);
        count++;
    }
}

void ThreadPlanner::deleteThreads(unsigned int n)
{
    while(count!=n)
    {
        //delete thread object
        count--;
    }
}

void ThreadPlanner::setData(LifeData *d)
{
    data = d;
}

void ThreadPlanner::start(int steps)
{
    if (data)
    {
        qDebug() << "calculations started";
        continueWorking = true;
        data->resetPointers();
        stepsCount = steps;
        notActiveThreadCount = 0;
        for(unsigned int i = 0; i < count; i++)
        {
            threads[i]->getThread()->start(QThread::NormalPriority);
            LifeThread::setData(data);
        }
    }
}

void ThreadPlanner::threadFinished()
{
    notActiveThreadCount++;
    qDebug() << "threads not working:" << notActiveThreadCount;
    if (notActiveThreadCount == count)
    {
        stepsCount--;
        emit stepFinished();
    }
}

void ThreadPlanner::nextStep()
{
    if (data)
    {
        if ((stepsCount != 0) && (continueWorking))
        {
            start(stepsCount);
        }
        else
        {
            emit stopped();
        }
    }
}

void ThreadPlanner::stop()
{
    continueWorking = false;
}
