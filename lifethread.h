#ifndef LIFETHREAD_H
#define LIFETHREAD_H

#include <QObject>
#include <lifedata.h>
#include <QPainter>

class LifeThread : public QObject
{
    Q_OBJECT
public:
    explicit LifeThread(QObject *parent = 0);
    static void setData(LifeData *d);
    void checkHorizontalBorder(register unsigned int *cellsToCheck, int tmpW, register unsigned int *neighborsNext, register unsigned int *neighborsPrev, int diffToBotCells, int chunkWidth, int y);
    void checkVerticalBorder(register unsigned int *cellsToCheck, int tmpW, register unsigned int *neighborsNext, register unsigned int *neighborsPrev, int diffToCells, int chunkWidth, int x);
signals:
    void finished();
public slots:
    void process();
private:
    int *in, *out;
    unsigned int height, width;
    unsigned int line;
    unsigned int itSize;
    static LifeData *data;
    QPainter painter;
};

#endif // LIFETHREAD_H
