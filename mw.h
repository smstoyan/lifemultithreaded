#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include <threadplanner.h>
#include <lifedata.h>
#include <lifewidget.h>

namespace Ui {
class MW;
}

class MW : public QMainWindow
{
    Q_OBJECT

public:
    explicit MW(QWidget *parent = 0);
    ~MW();

private slots:
    void on_startButton_clicked();

    void on_stepButton_clicked();

    void workEnded();

    void on_randomButton_clicked();

    void stopWork();

    void updateStepsStatus();
private:
    Ui::MW *ui;
    LifeWidget *lifeWidget;
    bool working;
    ThreadPlanner planner;
    LifeData data;
    unsigned int stepsProcessed;

    void startWork(int steps, int chunkSize, int threads);
};

#endif // MW_H
