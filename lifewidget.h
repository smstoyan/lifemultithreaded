#ifndef LIFEWIDGET_H
#define LIFEWIDGET_H

#include <QWidget>
#include <lifedata.h>
#include <QMouseEvent>
#include <QPainter>

class LifeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit LifeWidget(QWidget *parent = 0);

    void setData(LifeData *data);
    void setCellState(int x, int y, bool alive);
    void updateLifeState();
    void random(float v);
    void setPaintAccess(bool pAccess);

    virtual void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void paintEvent(QPaintEvent *);
signals:
    void repainted();
public slots:
    void repaint();
    void updateFromData();
private:
    LifeData *data;
    QPainter painter;
    QPixmap buffer;
    int cellSize;
    bool canPaint;

    bool checkCoordinates(int _x, int _y);
    void drawCell(int x, int y, bool alive);
};

#endif // LIFEWIDGET_H
